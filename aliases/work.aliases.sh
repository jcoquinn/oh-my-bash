#cd
alias cdpc='cd ~/projects/work/svn/prod/crons/programs'
alias cdpd='cd ~/projects/work/svn/devl/mygs/portal'
alias cdpp='cd ~/projects/work/svn/prod/mygs/portal'
alias cdps='cd ~/projects/work/svn/staging/mygs/portal'

alias cdmyd='cd ~/projects/work/svn/devl/mygs/'
alias cdmys='cd ~/projects/work/svn/staging/mygs/'
alias cdmyp='cd ~/projects/work/svn/prod/mygs/'

alias cdw='cd ~/projects/work'
alias cdwgh='cd ~/projects/work/github'
alias cdwgs='cd ~/projects/work/gsfinops'
alias cdwjc='cd ~/projects/work/jcoquinn'
alias cdws='cd ~/projects/work/svn'

# svn
alias svna='svn add'
alias svnd='svn diff'
alias svnl='svn log'
alias svns='svn status'
alias svnsu='svn status -u'
alias svnu='svn update'
